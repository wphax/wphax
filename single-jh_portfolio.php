<?php get_header(); ?>

        <div id="title">
            <div class="wrap">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>

        <div id="main" class="portfolio wrap">

            <div id="portfolio">
                <?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

                <article>
                    <?php
                    if( has_post_thumbnail() )the_post_thumbnail( 'portfolio-archive', array( 'class' => 'post-thumb' ) );
                    the_content();
                    ?>
                    <div class="clearfix"></div>
                    <p class="meta">Posted on <?php the_time('l, F jS, Y') ?> at <?php the_time() ?>.</p>
                </article>

                <?php endwhile; else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>
            </div>

            <div class="clearfix"></div>
        </div>

        <div id="cta" class="wrap">
            <p class="button left">
                <a href="#">Let's Work Together</a>
            </p>
            <p class="text right">Looking for a great looking website but don't have the budget of those huge companies, or is your business just starting up? Shoot me an email and I'll take care of you. :)</p>
            <div class="clearfix"></div>
        </div>

        <div id="reviews" class="wrap">
            <div id="user-reviews">
                <div class="review">
                    <p>"This is an example of what a review is going to look like on the homepage! It will rotate through a few others with a basic fading effect."</p>
                    <p class="author">&mdash; Jared Helgeson, <a href="http://wphax.com">wphax</a></p>
                </div>
            </div>
        </div>

<?php get_footer(); ?>