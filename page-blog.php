<?php get_header(); ?>

        <div id="title">
            <div class="wrap">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>

        <div id="main" class="blog wrap">

            <div id="blog-posts">
                <?php
                $query_args = array( 'post_type' => 'post' );
                $blog_query = new WP_Query( $query_args );
                if( $blog_query->have_posts() ) : while( $blog_query->have_posts() ) : $blog_query->the_post();
                ?>

                <article>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <p class="meta">Posted on <?php the_time('l, F jS, Y') ?> at <?php the_time() ?>.</p>
                    <?php
                    if( has_post_thumbnail() ) the_post_thumbnail( 'post-thumbnail', array( 'class' => 'post-thumb' ) );
                    the_excerpt();
                    ?>
                    <div class="clearfix"></div>
                </article>

                <?php endwhile; else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>
            </div>

            <?php get_sidebar(); ?>

            <div class="clearfix"></div>
        </div>

        <div id="cta" class="wrap">
            <p class="button left">
                <a href="#">Let's Work Together</a>
            </p>
            <p class="text right">Looking for a great looking website but don't have the budget of those huge companies, or is your business just starting up? Shoot me an email and I'll take care of you. :)</p>
            <div class="clearfix"></div>
        </div>

        <div id="reviews" class="wrap">
            <div id="user-reviews">
                <div class="review">
                    <p>"This is an example of what a review is going to look like on the homepage! It will rotate through a few others with a basic fading effect."</p>
                    <p class="author">&mdash; Jared Helgeson, <a href="http://wphax.com">wphax</a></p>
                </div>
            </div>
        </div>

<?php get_footer(); ?>