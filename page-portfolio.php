<?php
get_header();
?>

        <div id="title">
            <div class="wrap">
                <h1>Portfolio</h1>
            </div>
        </div>

        <div id="main" class="portfolio wrap">

            <?php
            $query = new WP_Query( array( 'post_type' => 'jh_portfolio' ) );
            if( $query->have_posts() ) : while( $query->have_posts() ) : $query->the_post();
            ?>

            <article>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <?php the_excerpt(); ?>
                <a href="<?php the_permalink(); ?>"><?php if( has_post_thumbnail() ) the_post_thumbnail( 'post-thumbnail', array( 'class' => 'post-thumb full-width' ) ); ?></a>
            </article>

            <?php endwhile; else: ?>
                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>

            <div class="clearfix"></div>
        </div>

        <div id="cta" class="wrap">
            <p class="button left">
                <a href="#">Let's Work Together</a>
            </p>
            <p class="text right">Looking for a great looking website but don't have the budget of those huge companies, or is your business just starting up? Shoot me an email and I'll take care of you. :)</p>
            <div class="clearfix"></div>
        </div>

        <div id="reviews" class="wrap">
            <div id="user-reviews">
                <div class="review">
                    <p>"This is an example of what a review is going to look like on the homepage! It will rotate through a few others with a basic fading effect."</p>
                    <p class="author">&mdash; Jared Helgeson, <a href="http://wphax.com">wphax</a></p>
                </div>
            </div>
        </div>

<?php get_footer(); ?>