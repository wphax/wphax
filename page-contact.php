<?php get_header(); ?>

        <div id="title">
            <div class="wrap">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>

        <div id="main" class="wrap">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <article>
                <div id="contact">
                    <aside>
                        <?php the_content(); ?>
                    </aside>
                    <form id="contact-form" action="" method="post">
                        <div class="row">
                            <div class="label"><label for="contact-name">Name</label></div>
                            <div class="field">
                                <input type="text" id="contact-name" name="contact[name]" value="" autofocus required />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row">
                            <div class="label"><label for="contact-email">Email</label></div>
                            <div class="field">
                                <input type="email" id="contact-email" name="contact[email]" value="" required />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row">
                            <div class="label"><label for="contact-subject">Subject</label></div>
                            <div class="field">
                                <input type="text" id="contact-subject" name="contact[subject]" value="" required />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row">
                            <div class="label"><label for="contact-message">Message</label></div>
                            <div class="field">
                                <textarea id="contact-message" name="contact[message]" required></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                &nbsp;
                            </div>
                            <div class="field" style="line-height: 1.3em;">
                                <input style="float: left; width: 25%;" type="submit" name="contact[submit]" value="Send" />
								<?php
								$message = $wphax->get_message();
								if( $message != '' )
									echo '<p style="float: right; width: 70%;"><em>' . $message . '</em></p>';
								?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </article>

            <?php endwhile; else: ?>
                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>
        </div>

        <div id="reviews" class="wrap">
            <div id="user-reviews">
                <div class="review">
                    <p>"This is an example of what a review is going to look like on the homepage! It will rotate through a few others with a basic fading effect."</p>
                    <p class="author">&mdash; Jared Helgeson, <a href="http://wphax.com">wphax</a></p>
                </div>
            </div>
        </div>

<?php get_footer(); ?>