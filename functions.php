<?php

if( !class_exists( 'wphax' ) ) {

	class wphax {

		private $message;

		public function __construct() {

			$this->message = '';

			require_once 'wp-less/wp-less.php';

			add_action( 'init', array( $this, 'init' ) );
			add_action( 'after_switch_theme', array( $this, 'after_switch_theme' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );

		}

		public function init() {

			add_theme_support( 'post-thumbnails' );
			add_image_size( 'portfolio-archive', 500, 300, true );

			register_nav_menus( array(
				'header-menu' => 'Header Menu'
			) );

			register_post_type( 'jh_portfolio',
				array(
					'labels'      => array(
						'name'          => __( 'Projects' ),
						'singular_name' => __( 'Project' )
					),
					'public'      => true,
					'has_archive' => true,
					'rewrite'     => array( 'slug' => 'project' ),
					'supports'    => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' )
				)
			);

			register_sidebar( array(
				'name'          => 'Blog Sidebar',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>',
				'before_widget' => '<div class="widget">',
				'after_widget'  => '</div>'
			) );

			if( isset( $_POST[ 'contact' ] ) ) {
				$contact = $_POST[ 'contact' ];
				$message = "From: " . $contact[ 'name' ] . " <" . $contact[ 'email' ] . ">\n";
				$message .= "Message:\n" . $contact[ 'message' ];

				$sent = wp_mail( get_option( 'admin_email' ), $contact[ 'subject' ], $message );
				if( $sent )
					$this->message = 'Your email has been sent! I\'ll get in touch with you ASAP!';

				if( !$sent )
					$this->message = 'There was a problem sending your email. Please try again later.';
			}

		}

		public function after_switch_theme() {
			flush_rewrite_rules();
		}

		public function wp_enqueue_scripts() {
			global $wp_version;

			if( !is_admin() ) {

				// Re-register jQuery so we can put it in the footer
				wp_deregister_script( 'jquery' );
				wp_register_script( 'jquery', get_bloginfo( 'template_url' ) . '/js/vendor/jquery-1.10.2.min.js', array( 'json2' ), $wp_version, true );

				// Header
				wp_enqueue_style( 'normalize', get_bloginfo( 'template_url' ) . '/css/normalize.css' );
				wp_enqueue_style( 'webfonts', get_bloginfo( 'template_url' ) . '/css/fonts.css' );
				wp_enqueue_style( 'main-style', get_bloginfo( 'template_url' ) . '/css/main.less' );

				wp_enqueue_script( 'modernizr', get_bloginfo( 'template_url' ) . '/js/vendor/modernizr-2.6.2.min.js' );
				wp_enqueue_script( 'less', get_bloginfo( 'template_url' ) . '/js/less-1.4.1.min.js' );

				// Footer
				wp_enqueue_script( 'jquery' );
				wp_enqueue_script( 'plugins', get_bloginfo( 'template_url' ) . '/js/plugins.js', array( 'jquery' ), $wp_version, true );
				wp_enqueue_script( 'main-script', get_bloginfo( 'template_url' ) . '/js/main.js', array( 'jquery' ), $wp_version, true );

			}
		}

		public function get_message() {
			return $this->message;
		}

	}

}

global $wphax;
$wphax = new wphax();