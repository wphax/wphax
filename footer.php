<footer>
    <div class="wrap">
        <p>Copyright &copy; 2013 <?php bloginfo( 'name' ); ?> - All Rights Reserved</p>
        <p><a href="#">SEO</a> and <a href="#">Website Design</a> by <a href="http://wphax.com">wphax</a></p>
        <p>iPad&reg;, iPhone&reg; and iPod&reg; are a trademark of Apple Inc. Android is a trademark of Google Inc.</p>
    </div>
</footer>

<?php wp_footer(); ?>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
</script>
</body>
</html>