<?php get_header(); ?>

        <div id="title">
            <div class="home wrap">
                <div class="left">
                    <h1>Responsive web design is the web's latest standard</h1>
                    <p>A designer knows he has achieved perfection not when there is nothing left to add, but when there is nothing left to take away.<br />
                        &mdash; Antoine de Saint-Exupery </p>
                </div>
                <div class="right">
                    <img src="<?php bloginfo( 'template_url' ); ?>/test.jpg" />
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div id="cta" class="wrap">
            <p class="button left">
                <a href="#">Let's Work Together</a>
            </p>
            <p class="text right">Looking for a great looking website but don't have the budget of those huge companies, or is your business just starting up? Shoot me an email and I'll take care of you. :)</p>
            <div class="clearfix"></div>
        </div>

        <div id="details" class="wrap">
            <div class="columns">
                <div class="col col3">
                    <h2>Responsive Design</h2>
                    <p class="img"><img src="<?php bloginfo( 'template_url' ); ?>/img/responsive.png" width="79" height="79" alt="Responsive Design" /></p>
                    <p>With everyone requiring a responsive web design these days, we know how important it is not only for the user, but for web standards as well. Every one of our designs has carefully selected viewports and designs that look great on all devices.</p>
                </div>
                <div class="col col3">
                    <h2>Easy-to-Use Backend</h2>
                    <p class="img"><img src="<?php bloginfo( 'template_url' ); ?>/img/backend.png" width="79" height="79" alt="Easy-to-Use Backend" /></p>
                    <p>We also include in every one of our WordPress sites an easy to use backend to customize your website. It leverages the new WordPress Customize feature to customize your website's design on-the-fly and publish it when you are fully satisfied.</p>
                </div>
                <div class="col col3">
                    <h2>Always on Deadline</h2>
                    <p class="img"><img src="<?php bloginfo( 'template_url' ); ?>/img/deadline.png" width="79" height="79" alt="Always on Deadline" /></p>
                    <p>We give ourselves plenty of room to surprise you with an early-finished product, however even for those tight deadlines, we're up for the challenge and guarantee that any timeframe we quote will be the absolute maximum amount of time spent on your project.</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div id="reviews" class="wrap">
            <div id="user-reviews">
                <div class="review">
                    <p>"This is an example of what a review is going to look like on the homepage! It will rotate through a few others with a basic fading effect."</p>
                    <p class="author">&mdash; Jared Helgeson, <a href="http://wphax.com">wphax</a></p>
                </div>
            </div>
        </div>

<?php get_footer(); ?>
